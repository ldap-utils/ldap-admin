# LDAP admin=

There are different ways to administer the LDAP Provider
  - On the command line
  - [[ https://directory.apache.org/studio/ | Apache directory studio ]] is a great tool for Admins.
  - Our LDAP admin [[ https://gitlab.com/femprocomuns/ldapclient | web app ]] 
  - Our onboarding web app



## Our LDAP admin (an ldap client)

This is a quick [[ https://gitlab.com/femprocomuns/ldapclient | web interface]] made to fit our basic commonscloud config options.
The client is programmed in python2.7 using [[ http://flask.pocoo.org/ | Flask ]] a python microframework, [[ http://gunicorn.org/ | Gunicorn ]] a web server, and [[ http://supervisord.org/introduction.html | Supervisor  ]] to keep it running.


## Install the client
//It is not necessary to install the client on the same server as the ldap server.//
```
apt-get install libjpeg62-turbo libjpeg62-turbo-dev
sudo apt-get install -y python-dev libldap2-dev libsasl2-dev libssl-dev
apt-get install python-ldap git python-virtualenv python-pip 

mkdir /opt/ldap
cd /opt/ldap
git clone https://gitlab.com/femprocomuns/ldapclient.git
virtualenv ./venv
source ./venv/bin/activate
pip install flask flask_wtf flask_login flask_script
pip install python-ldap
pip install randomavatar
pip install gunicorn

cp /opt/ldap/ldapclient/config.cfg.example /opt/ldap/ldapclient/config.cfg
```
Edit `/opt/ldap/ldapclient/config.cfg`

Edit `/opt/ldap/manage_ldapclient.py`


```
from flask_script import Manager, Server
from ldapclient import app

manager = Manager(app)

# Turn on debugger by default and reloader
manager.add_command("run", Server(
    use_debugger = True,
    use_reloader = True,
    threaded = True,
    port = '5001',
    host = '0.0.0.0')
)

if __name__ == "__main__":
    manager.run()

```
==Configure gunicorn==
Edit `/opt/ldap/gunicorn-ldapclient.conf`

```
command = '/opt/ldap/venv/bin/gunicorn'
pythonpath = '/opt/ldap'
bind = '0.0.0.0:5001'
workers = 3
user = 'www-data'
```


## Install and configure supervisor
```
apt-get install supervisor
```
Edit `/etc/supervisor/conf.d/ldapclient.conf`
```
[program:ldapclient]
command = /opt/ldap/venv/bin/gunicorn -c /opt/ldap/gunicorn-ldapclient.conf ldapclient:app
directory = /opt/ldap
user = www-data
```
```
/etc/init.d/supervisor restart
```
Now you can stop, start and check ldapclient status.
```
supervisorctl start|stop|status ldapclient
```

# nginx

We need an nginx proxy somewhere

```
apt-get install nginx
```

Follow the [[ https://certbot.eff.org/lets-encrypt/ubuntuxenial-nginx | letsencrypt instructions ]] to install a cert.

Edit `/etc/nginx/sites-available/ldapclient`


```
To come..
```
